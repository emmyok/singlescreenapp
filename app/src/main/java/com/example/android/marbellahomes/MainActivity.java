package com.example.android.marbellahomes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMenuBtnClick(View view) {
        LinearLayout menu =(LinearLayout) findViewById(R.id.menu);
        if(menu.getVisibility()==View.VISIBLE){
            menu.setVisibility(View.INVISIBLE);
        }
        else {
            menu.setVisibility(View.VISIBLE);
        }
    }
}
